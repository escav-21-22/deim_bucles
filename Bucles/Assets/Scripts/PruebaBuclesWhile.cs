using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaBuclesWhile : MonoBehaviour
{
    public int contador;

    private void Start()
    {
        /* En los bucles while, debemos incluir la siguiente configuraci�n:
         * int i = __ --> Se declara la variable que utilizamos para la iteraci�n antes de empezar el bucle
         * while (i <= __) --> Se indica la condici�n que debe cumplirse para seguir ejecut�ndose el bucle 
         * i++ --> En el interior del bucle, se indica el incremento de la variable en cada una de las iteraciones
         */

        // Los dos bucles siguientes se ejecutan 50 veces, mostrando por tanto 50 veces "hola" cada uno
        int i = 0;
        while (i < 50)
        {
            Debug.Log("hola");
            i++;
        }
        int j = 1;
        while (j <= 50)
        {
            Debug.Log("hola");
            j++;
        }

        // Los dos bucles siguientes se ejecutan las veces indicadas en la variable "contador"
        int k = 0;
        while (k < contador)
        {
            Debug.Log("hola");
            k++;
        }
        int l = 1;
        while (l <= contador)
        {
            Debug.Log("hola");
            l++;
        }

        // El siguiente bucle imprime por consola los n�meros del 1 al 30
        int m = 0;
        while (m < 30)
        {
            Debug.Log(m);
            m++;
        }
        // El siguiente bucle imprime por consola los n�meros del 1 al n�mero que contenga la variable "contador"
        int n = 0;
        while (n < contador)
        {
            Debug.Log(n);
            n++;
        }

        // El siguiente bucle imprime por consola los n�meros impares menores que el n�mero que contanga la variable "contador"
        int o = 0;
        while (o < contador)
        {
            if (o % 2 != 0)
            {
                Debug.Log(o);
            }
            o++;
        }
    }
}