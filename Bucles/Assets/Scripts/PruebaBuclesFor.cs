using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaBuclesFor : MonoBehaviour
{
    public int contador;

    private void Start()
    {
        /* En los bucles for, encontramos 3 par�metros que son configurables:
         * int i = __ --> Se indica el valor inicial de la variable que utilizamos para la iteraci�n
         * i <= __ --> Se indica la condici�n que debe cumplirse para seguir ejecut�ndose el bucle 
         * i++ --> Se indica el incremento de la variable en cada una de las iteraciones
         */

        // Los dos bucles siguientes se ejecutan 50 veces, mostrando por tanto 50 veces "hola" cada uno
        for (int i = 0; i < 50; i++)
        {
            Debug.Log("hola");
        }
        for (int i = 1; i <= 50; i++)
        {
            Debug.Log("hola");
        }

        // Los dos bucles siguientes se ejecutan las veces indicadas en la variable "contador"
        for (int i = 0; i < contador; i++)
        {
            Debug.Log("hola");
        }
        for (int i = 1; i <= contador; i++)
        {
            Debug.Log("hola");
        }

        // El siguiente bucle imprime por consola los n�meros del 1 al 30
        for (int i = 1; i <= 30; i++)
        {
            Debug.Log(i);
        }
        // El siguiente bucle imprime por consola los n�meros del 1 al n�mero que contenga la variable "contador"
        for (int i = 1; i <= contador; i++)
        {
            Debug.Log(i);
        }

        // El siguiente bucle imprime por consola los n�meros impares menores que el n�mero que contanga la variable "contador"
        for (int i = 1; i <= contador; i++)
        {
            if (i % 2 != 0)
            {
                Debug.Log(i);
            }
        }

    }
}
